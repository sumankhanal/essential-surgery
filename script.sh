# Bash script to change extension of Rmd to qmd

for file in *.Rmd; do
    mv -- "$file" "${file%.Rmd}.qmd"
done