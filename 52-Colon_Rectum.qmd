# Colon and Rectum


### Question

**Write on risk factors for colon cancer**


**Modifiable factors**

1. Excess body weight
2. Physical inactivity. Physical activity decreases the risk of colon but not rectal cancer.
3. Long-term smoking
4. Heavy alcohol consumption
5. High consumption of red or processed meat, starchy foods, refined carbohydrates, and sugary drinks
6. Low calcium intake
7. Very low intake of fruits and vegetables and whole-grain fiber
8. Renal transplant under immunosuppression
9. Diabetes and insulin resistance: relative risk [RR] 1.38


**Non-modifiable factors**

1. Increasing age
2. Personal or family history of colorectal cancer and/or adenomatous polyps
3. Inherited genetic conditions (e.g., FAP, HNPCC (Lynch syndrome), MUTYH-associated polyposis (MAP), Hereditary breast and ovarian cancer syndrome)
4. Personal history of chronic inflammatory bowel disease (ulcerative colitis or Crohn’s disease)
5. Type 2 diabetes
6. Race: African Americans have the highest CRC rates
7. Gender: males are at high risk


Regular long-term use of NSAIDS, such as aspirin, reduces risk, but at a cost of upper GI bleeding.



### Question

**What is the association of diabetes with colorectal cancer?**

The risk of colorectal cancer is estimated to be 27% higher in patients with type 2 DM than in non-diabetic controls. Although evidence is limited, insulin use has been associated with increased and metformin with decreased incidence of colorectal cancer. In addition, colorectal cancer shares some cellular and molecular pathways with diabetes target organ damage, exemplified by diabetic kidney disease. These include epithelial cell injury, activation of inflammation and Wnt/β-catenin pathways and iron homeostasis defects, among others. Genome-wide association studies have identified diabetes-associated genes (e.g. TCF7L2) that may also contribute to colorectal cancer.[^1]






[^1]: https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5392343/




